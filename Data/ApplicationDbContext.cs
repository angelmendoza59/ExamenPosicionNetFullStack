﻿using Duende.IdentityServer.EntityFramework.Options;
using Entities;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.AspNetCore.Identity;

namespace Data
{
    public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IOptions<OperationalStoreOptions> operationalStoreOptions)
            : base(options, operationalStoreOptions)
        {
        }
        public DbSet<IdentityRole> Roles { get; set; }
        public DbSet<AspNetUserCliente> AspNetUserClientes { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Articulo> Articulos { get; set; }
        public DbSet<Tienda> Tiendas { get; set; }
        public DbSet<ArticuloTienda> ArticuloTiendas { get; set; }
        public DbSet<ClienteArticulo> ClienteArticulos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Articulo>()
                .Property(a => a.Precio)
                .HasColumnType("decimal(18,2)");

            modelBuilder.Entity<ClienteArticulo>()
                .HasKey(ca => ca.Id);

            modelBuilder.Entity<ClienteArticulo>()
                .HasOne(ca => ca.Cliente)
                .WithMany(c => c.ClienteArticulos)
                .HasForeignKey(ca => ca.ClienteId)
                .HasPrincipalKey(c => c.Id) // Especificar la clave principal
                .IsRequired();
            modelBuilder.Entity<AspNetUserCliente>()
            .HasKey(uc => new { uc.AspNetUserId, uc.ClienteId });

            modelBuilder.Entity<AspNetUserCliente>()
                .HasOne(uc => uc.AspNetUser)
                .WithMany()
                .HasForeignKey(uc => uc.AspNetUserId)
                .IsRequired();

            modelBuilder.Entity<AspNetUserCliente>()
                .HasOne(uc => uc.Cliente)
                .WithMany()
                .HasForeignKey(uc => uc.ClienteId)
                .IsRequired();

        }
    }

}