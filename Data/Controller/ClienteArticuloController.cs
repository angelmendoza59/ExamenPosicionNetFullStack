﻿using Business.Interface;
using Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Controller
{
    [ApiController]
    [Route("api/[controller]")]
    public class ClienteArticuloController : ControllerBase
    {
        private readonly IRepository<ClienteArticulo> _clienteArticuloRepository;

        public ClienteArticuloController(IRepository<ClienteArticulo> clienteArticuloRepository)
        {
            _clienteArticuloRepository = clienteArticuloRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllClienteArticulos()
        {
            var clienteArticulos = await _clienteArticuloRepository.GetAll();
            return Ok(clienteArticulos);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetClienteArticuloById(int id)
        {
            var clienteArticulo = await _clienteArticuloRepository.GetById(id);
            if (clienteArticulo == null)
                return NotFound();

            return Ok(clienteArticulo);
        }

        [HttpPost]
        public async Task<IActionResult> AddClienteArticulo(List<ClienteArticulo> clienteArticulos)
        {
            foreach (var clienteArticulo in clienteArticulos)
            {
                await _clienteArticuloRepository.Add(clienteArticulo);
            }
            return CreatedAtAction(nameof(GetClienteArticuloById), new { id = clienteArticulos[0].Id }, clienteArticulos);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateClienteArticulo(int id, ClienteArticulo clienteArticulo)
        {
            if (id != clienteArticulo.Id)
                return BadRequest();

            await _clienteArticuloRepository.Update(clienteArticulo);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClienteArticulo(int id)
        {
            var clienteArticulo = await _clienteArticuloRepository.GetById(id);
            if (clienteArticulo == null)
                return NotFound();

            await _clienteArticuloRepository.Delete(clienteArticulo);
            return NoContent();
        }
    }
}
