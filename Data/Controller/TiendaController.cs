﻿using Business;
using Data.Repository;
using Entities;
using Microsoft.AspNetCore.Mvc;

namespace Data.Controller
{
    [ApiController]
    [Route("api/[controller]")]
    public class TiendaController : ControllerBase
    {
        private readonly TiendaRepository _tiendaRepository;

        public TiendaController(TiendaRepository tiendaRepository)
        {
           _tiendaRepository = tiendaRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllTiendas()
        {
            var tiendas = await _tiendaRepository.GetAll();
            return Ok(tiendas);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTiendaById(int id)
        {
            var tienda = await _tiendaRepository.GetById(id);
            if (tienda == null)
                return NotFound();

            return Ok(tienda);
        }

        [HttpPost]
        public async Task<IActionResult> AddTienda(Tienda tienda)
        {
            await _tiendaRepository.Add(tienda);
            return CreatedAtAction(nameof(GetTiendaById), new { id = tienda.Id }, tienda);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateTienda(int id, Tienda tienda)
        {
            if (id != tienda.Id)
                return BadRequest();

            await _tiendaRepository.Update(tienda);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTienda(int id)
        {
            var tienda = await _tiendaRepository.GetById(id);
            if (tienda == null)
                return NotFound();

            await _tiendaRepository.Delete(tienda);
            return NoContent();
        }
    }

}
