﻿using Entities;
using Business;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Data.Repository;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Data.Controller
{
    [ApiController]
    [Route("api/[controller]")]
    public class ClienteController : ControllerBase
    {
        private readonly ClienteRepository _clienteRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _dbContext;

        public ClienteController(ClienteRepository clienteRepository, UserManager<ApplicationUser> userManager, ApplicationDbContext dbContext)
        {
           _clienteRepository = clienteRepository;
           _userManager = userManager;
           _dbContext = dbContext;
        }
        [HttpGet("getclientesbyemail")]
        public async Task<IActionResult> GetClientesByEmail(string email)
        {
            try
            {
                // Obtener el usuario con el correo electrónico dado
                var user = await _userManager.FindByEmailAsync(email);

                if (user == null)
                {
                    // No se encontró un usuario con el correo electrónico dado
                    return NotFound();
                }

                // Obtener los clientes asociados al usuario a través de la tabla AspNetUserClientes
                var clientes = _dbContext.AspNetUserClientes
                    .Where(uc => uc.AspNetUserId == user.Id)
                    .Select(uc => uc.ClienteId)
                    .ToList();

                // Devolver la lista de clientes encontrados
                return Ok(clientes);
            }
            catch (Exception ex)
            {
                // Ocurrió un error durante la obtención de los clientes
                return StatusCode(500, $"Error al obtener los clientes: {ex.Message}");
            }
        }
        [HttpGet("getclientesbyemailname")]
        public async Task<IActionResult> GetClientesByEmailName(string email)
        {
            try
            {
                // Obtener el usuario con el correo electrónico dado
                var user = await _userManager.FindByEmailAsync(email);

                if (user == null)
                {
                    // No se encontró un usuario con el correo electrónico dado
                    return NotFound();
                }

                // Obtener los clientes asociados al usuario a través de la tabla AspNetUserClientes
                var clienteIds = _dbContext.AspNetUserClientes
                    .Where(uc => uc.AspNetUserId == user.Id)
                    .Select(uc => uc.ClienteId)
                    .ToList();

                // Obtener los nombres de los clientes correspondientes a los clienteIds
                var clientes = _dbContext.Clientes
                    .Where(c => clienteIds.Contains(c.Id))
                    .Select(c => new { c.Id, c.Nombre })
                    .ToList();

                // Devolver la lista de clientes encontrados
                return Ok(clientes);
            }
            catch (Exception ex)
            {
                // Ocurrió un error durante la obtención de los clientes
                return StatusCode(500, $"Error al obtener los clientes: {ex.Message}");
            }
        }
        [HttpGet]
        public async Task<IActionResult> GetAllClientes()
        {
            var clientes = await _clienteRepository.GetAll();
            return Ok(clientes);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetClienteById(int id)
        {
            var cliente = await _clienteRepository.GetById(id);
            if (cliente == null)
                return NotFound();

            return Ok(cliente);
        }

        [HttpPost]
        public async Task<IActionResult> AddCliente(Cliente cliente)
        {
            await _clienteRepository.Add(cliente);
            return CreatedAtAction(nameof(GetClienteById), new { id = cliente.Id }, cliente);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCliente(int id, Cliente cliente)
        {
            if (id != cliente.Id)
                return BadRequest();

            await _clienteRepository.Update(cliente);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCliente(int id)
        {
            var cliente = await _clienteRepository.GetById(id);
            if (cliente == null)
                return NotFound();

            await _clienteRepository.Delete(cliente);
            return NoContent();
        }
    }
}
