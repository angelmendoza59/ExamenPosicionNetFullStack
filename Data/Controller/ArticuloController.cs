﻿using Business;
using Data.Repository;
using Entities;
using Microsoft.AspNetCore.Mvc;


namespace Data.Controller
{
    [ApiController]
    [Route("api/[controller]")]
    public class ArticuloController : ControllerBase
    {
        private readonly ArticuloRepository _articuloRepository;

        public ArticuloController(ArticuloRepository articuloRepository)
        {
            _articuloRepository = articuloRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllArticulos()
        {
            var articulos = await _articuloRepository.GetAll();
            return Ok(articulos);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetArticuloById(int id)
        {
            var articulo = await _articuloRepository.GetById(id);
            if (articulo == null)
                return NotFound();

            return Ok(articulo);
        }

        [HttpPost]
        public async Task<IActionResult> AddArticulo(Articulo articulo)
        {
            await _articuloRepository.Add(articulo);
            return CreatedAtAction(nameof(GetArticuloById), new { id = articulo.Id }, articulo);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateArticulo(int id, Articulo articulo)
        {
            if (id != articulo.Id)
                return BadRequest();

            await _articuloRepository.Update(articulo);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteArticulo(int id)
        {
            var articulo = await _articuloRepository.GetById(id);
            if (articulo == null)
                return NotFound();

            await _articuloRepository.Delete(articulo);
            return NoContent();
        }
    }
}
