﻿using Business.Interface;
using Data.Repository;
using Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Controller
{
    [ApiController]
    [Route("api/[controller]")]
    public class ArticuloTiendaController : ControllerBase
    {
        private readonly IRepository<ArticuloTienda> _articuloTiendaRepository;
        private readonly IRepository<Tienda> _tiendaRepository;
        private readonly IRepository<Articulo> _articuloRepository;

        public ArticuloTiendaController(IRepository<ArticuloTienda> articuloTiendaRepository, IRepository<Tienda> tiendaRepository, IRepository<Articulo> articuloRepository)
        {
            _articuloTiendaRepository = articuloTiendaRepository;
            _tiendaRepository = tiendaRepository;
            _articuloRepository = articuloRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var articuloTiendas = await _articuloTiendaRepository.GetAll();
            var tienda = await _tiendaRepository.GetAll();
            var articulo = await _articuloRepository.GetAll();
            var response = articuloTiendas.Select(at => new
            {
                at.Id,
                at.ArticuloId,
                at.TiendaId,
                at.Fecha,
                Articulo = new
                {
                    at.Articulo.Id,
                    at.Articulo.Codigo,
                    at.Articulo.Descripcion,
                    at.Articulo.Precio,
                    at.Articulo.Imagen,
                    at.Articulo.Stock
                },
                Tienda = new
                {
                    at.Tienda.Id,
                    at.Tienda.Sucursal,
                    at.Tienda.Direccion
                }
            });

            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var articuloTienda = await _articuloTiendaRepository.GetById(id);
            if (articuloTienda == null)
            {
                return NotFound();
            }

            return Ok(articuloTienda);
        }

        
        [HttpPost]
        public async Task<IActionResult> Create(ArticuloTienda articuloTiendaDto)
        {
            // Verificar si la tienda existe
            var tienda = await _tiendaRepository.GetById(articuloTiendaDto.TiendaId);
            if (tienda == null)
            {
                return NotFound("La tienda especificada no existe");
            }

            // Verificar si el artículo existe
            var articulo = await _articuloRepository.GetById(articuloTiendaDto.ArticuloId);
            if (articulo == null)
            {
                return NotFound("El artículo especificado no existe");
            }

            await _articuloTiendaRepository.Add(articuloTiendaDto);

            return CreatedAtAction(nameof(GetById), new { id = articuloTiendaDto.Id }, articuloTiendaDto);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, ArticuloTienda articuloTiendaDto)
        {
            var articuloTienda = await _articuloTiendaRepository.GetById(id);
            if (articuloTienda == null)
            {
                return NotFound();
            }

            // Verificar si la tienda existe
            var tienda = await _tiendaRepository.GetById(articuloTiendaDto.TiendaId);
            if (tienda == null)
            {
                return NotFound("La tienda especificada no existe");
            }

            // Verificar si el artículo existe
            var articulo = await _articuloRepository.GetById(articuloTiendaDto.ArticuloId);
            if (articulo == null)
            {
                return NotFound("El artículo especificado no existe");
            }

            // Actualizar la relación entre la tienda y el artículo
            articuloTienda.TiendaId = articuloTiendaDto.TiendaId;
            articuloTienda.ArticuloId = articuloTiendaDto.ArticuloId;

            await _articuloTiendaRepository.Update(articuloTienda);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var articuloTienda = await _articuloTiendaRepository.GetById(id);
            if (articuloTienda == null)
            {
                return NotFound();
            }

            await _articuloTiendaRepository.Delete(articuloTienda);

            return NoContent();
        }
    }

}
