﻿using Business.Interface;
using Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ClienteArticuloRepository : IRepository<ClienteArticulo>
    {
        private readonly ApplicationDbContext _dbContext;

        public ClienteArticuloRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<ClienteArticulo>> GetAll()
        {
            return await _dbContext.Set<ClienteArticulo>().ToListAsync();
        }

        public async Task<ClienteArticulo> GetById(int id)
        {
            return await _dbContext.Set<ClienteArticulo>().FindAsync(id);
        }

        public async Task Add(ClienteArticulo entity)
        {
            _dbContext.Set<ClienteArticulo>().Add(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Update(ClienteArticulo entity)
        {
            _dbContext.Set<ClienteArticulo>().Update(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(ClienteArticulo entity)
        {
            _dbContext.Set<ClienteArticulo>().Remove(entity);
            await _dbContext.SaveChangesAsync();
        }
    }
}
