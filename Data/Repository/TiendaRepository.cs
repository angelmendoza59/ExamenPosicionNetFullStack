﻿
using Business.Interface;
using Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class TiendaRepository : IRepository<Tienda>
    {
        private readonly ApplicationDbContext _dbContext;

        public TiendaRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Tienda>> GetAll()
        {
            return await _dbContext.Tiendas.ToListAsync();
        }

        public async Task<Tienda> GetById(int id)
        {
            return await _dbContext.Tiendas.FindAsync(id);
        }

        public async Task Add(Tienda tienda)
        {
            _dbContext.Tiendas.Add(tienda);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Update(Tienda tienda)
        {
            _dbContext.Entry(tienda).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(Tienda tienda)
        {
            _dbContext.Tiendas.Remove(tienda);
            await _dbContext.SaveChangesAsync();
        }
    }
}
