﻿using Business.Interface;
using Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ArticuloTiendaRepository : IRepository<ArticuloTienda>
    {
        private readonly ApplicationDbContext _dbContext;

        public ArticuloTiendaRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<ArticuloTienda>> GetAll()
        {
            return await _dbContext.Set<ArticuloTienda>().ToListAsync();
        }

        public async Task<ArticuloTienda> GetById(int id)
        {
            return await _dbContext.Set<ArticuloTienda>().FindAsync(id);
        }

        public async Task Add(ArticuloTienda entity)
        {
            _dbContext.Set<ArticuloTienda>().Add(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Update(ArticuloTienda entity)
        {
            _dbContext.Set<ArticuloTienda>().Update(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(ArticuloTienda entity)
        {
            _dbContext.Set<ArticuloTienda>().Remove(entity);
            await _dbContext.SaveChangesAsync();
        }
    }
}
