﻿using Business.Interface;
using Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repository
{
    public class ArticuloRepository : IRepository<Articulo>
    {
        private readonly ApplicationDbContext _dbContext;

        public ArticuloRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Articulo>> GetAll()
        {
            return await _dbContext.Articulos.ToListAsync();
        }

        public async Task<Articulo> GetById(int id)
        {
            return await _dbContext.Articulos.FindAsync(id);
        }

        public async Task Add(Articulo articulo)
        {
            _dbContext.Articulos.Add(articulo);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Update(Articulo articulo)
        {
            _dbContext.Entry(articulo).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(Articulo articulo)
        {
            _dbContext.Articulos.Remove(articulo);
            await _dbContext.SaveChangesAsync();
        }
    }
}
