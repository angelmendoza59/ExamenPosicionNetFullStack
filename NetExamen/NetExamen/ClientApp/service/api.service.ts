import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ArticuloTienda } from 'src/app/articulotienda/articulo-tienda.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private baseUrl = 'https://localhost:7161/api'; // Reemplaza con la URL base de tu API

  constructor(private http: HttpClient) { }

  // Métodos para el controlador de Tienda

  getTiendas(): Observable<any> {
    const url = `${this.baseUrl}/tienda`;
    return this.http.get(url);
  }

  getTiendaById(id: number): Observable<any> {
    const url = `${this.baseUrl}/tienda/${id}`;
    return this.http.get(url);
  }

  addTienda(tienda: any): Observable<any> {
    const url = `${this.baseUrl}/tienda`;
    return this.http.post(url, tienda);
  }

  updateTienda(id: number, tienda: any): Observable<any> {
    const url = `${this.baseUrl}/tienda/${id}`;
    return this.http.put(url, tienda);
  }

  deleteTienda(id: number): Observable<any> {
    const url = `${this.baseUrl}/tienda/${id}`;
    return this.http.delete(url);
  }

  // Métodos para el controlador de Articulo

  getArticulos(): Observable<any> {
    const url = `${this.baseUrl}/articulo`;
    return this.http.get(url);
  }

  getArticuloById(id: number): Observable<any> {
    const url = `${this.baseUrl}/articulo/${id}`;
    return this.http.get(url);
  }

  addArticulo(articulo: any): Observable<any> {
    const url = `${this.baseUrl}/articulo`;
    return this.http.post(url, articulo);
  }

  updateArticulo(id: number, articulo: any): Observable<any> {
    const url = `${this.baseUrl}/articulo/${id}`;
    return this.http.put(url, articulo);
  }

  deleteArticulo(id: number): Observable<any> {
    const url = `${this.baseUrl}/articulo/${id}`;
    return this.http.delete(url);
  }

  // Métodos para el controlador de Cliente

  getClientes(): Observable<any> {
    const url = `${this.baseUrl}/cliente`;
    return this.http.get(url);
  }

  getClienteById(id: number): Observable<any> {
    const url = `${this.baseUrl}/cliente/${id}`;
    return this.http.get(url);
  }

  addCliente(cliente: any): Observable<any> {
    const url = `${this.baseUrl}/cliente`;
    return this.http.post(url, cliente);
  }

  updateCliente(id: number, cliente: any): Observable<any> {
    const url = `${this.baseUrl}/cliente/${id}`;
    return this.http.put(url, cliente);
  }

  deleteCliente(id: number): Observable<any> {
    const url = `${this.baseUrl}/cliente/${id}`;
    return this.http.delete(url);
  }
  getAllArticuloTiendas(): Observable<any> {
    const url = `${this.baseUrl}/articulotienda`;
    return this.http.get(url);
  }
  
  getArticuloTiendaById(id: number): Observable<any> {
    const url = `${this.baseUrl}/articulotienda/${id}`;
    return this.http.get(url);
  }
  
  addArticuloTienda(articuloTienda: any): Observable<any> {
    const url = `${this.baseUrl}/articulotienda`;
    return this.http.post(url, articuloTienda);
  }
  
  updateArticuloTienda(id: number, articuloTienda: any): Observable<any> {
    const url = `${this.baseUrl}/articulotienda/${id}`;
    return this.http.put(url, articuloTienda);
  }
  
  deleteArticuloTienda(id: number): Observable<any> {
    const url = `${this.baseUrl}/articulotienda/${id}`;
    return this.http.delete(url);
  }
  getAllClienteArticulos(): Observable<any> {
    const url = `${this.baseUrl}/clientearticulo`;
    return this.http.get(url);
  }
  
  getClienteArticuloById(id: number): Observable<any> {
    const url = `${this.baseUrl}/clientearticulo/${id}`;
    return this.http.get(url);
  }
  
  addClienteArticulo(clienteArticulos: any[]): Observable<any> {
    const url = `${this.baseUrl}/clientearticulo`;
    return this.http.post(url, clienteArticulos);
  }
  
  updateClienteArticulo(clienteArticulo: any): Observable<any> {
    const url = `${this.baseUrl}/clientearticulo/${clienteArticulo.id}`;
    return this.http.put(url, clienteArticulo);
  }
  
  deleteClienteArticulo(id: number): Observable<any> {
    const url = `${this.baseUrl}/clientearticulo/${id}`;
    return this.http.delete(url);
  }
  getClientesByEmail(email: string): Observable<any> {
    const url = `${this.baseUrl}/cliente/getclientesbyemail?email=${email}`;
    return this.http.get(url);
  }
  getClientesByEmailName(email: string): Observable<any> {
    const url = `${this.baseUrl}/cliente/getclientesbyemailname?email=${email}`;
    return this.http.get(url);
  }
  
}
