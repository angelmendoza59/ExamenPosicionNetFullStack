import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { ApiAuthorizationModule } from 'src/api-authorization/api-authorization.module';
import { AuthorizeGuard } from 'src/api-authorization/authorize.guard';
import { AuthorizeInterceptor } from 'src/api-authorization/authorize.interceptor';
import { ClienteComponent } from './cliente/cliente.component';
import { ArticuloComponent } from './articulo/articulo.component';
import { TiendaComponent } from './tienda/tienda.component';
import { ArticuloTiendaComponent } from './articulotienda/articulotienda.component';
import { ClienteArticuloComponent } from './clientearticulo/clientearticulo.component';
import { CarritoComprasComponent } from './carritocompras/carritocompras.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    ClienteComponent,
    ArticuloComponent,
    TiendaComponent,
    ArticuloTiendaComponent,
    ClienteArticuloComponent,
    CarritoComprasComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ApiAuthorizationModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent, canActivate: [AuthorizeGuard] },
      { path: 'clientes', component: ClienteComponent },
      { path: 'tiendas', component: TiendaComponent },
      { path: 'articulos', component: ArticuloComponent },
      { path: 'articulostienda', component: ArticuloTiendaComponent },
      { path: 'clientearticulo', component: ClienteArticuloComponent },
      { path: 'carritocompras', component: CarritoComprasComponent },
    ])
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthorizeInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
