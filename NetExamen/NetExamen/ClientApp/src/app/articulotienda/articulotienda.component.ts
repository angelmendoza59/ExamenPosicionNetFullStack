import { Component, OnInit } from '@angular/core';
import { ApiService } from 'service/api.service';
import { ArticuloTienda } from './articulo-tienda.model';


@Component({
  selector: 'app-articulo-tienda',
  templateUrl: './articulotienda.component.html',
  styleUrls: ['./articulotienda.component.css']
})
export class ArticuloTiendaComponent implements OnInit {
  articuloTiendas: ArticuloTienda[] = [];
  nuevoArticuloTienda: ArticuloTienda = {
    id: 0,
    articuloId: 0,
    tiendaId: 0,
    fecha: new Date(),
    
  };

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.getAllArticuloTiendas();
  }

  getAllArticuloTiendas(): void {
    this.apiService.getAllArticuloTiendas().subscribe(
      (response) => {
        this.articuloTiendas = response; // Asignar la respuesta directamente a articuloTiendas
      },
      (error) => {
        console.error(error);
      }
    );
  }

  addArticuloTienda(): void {
    this.apiService.addArticuloTienda(this.nuevoArticuloTienda).subscribe(
      (response) => {
        console.log(response);
        // Actualizar la lista de articulo-tiendas
        this.getAllArticuloTiendas();
        // Limpiar el formulario
        this.nuevoArticuloTienda = {
          id: 0,
          articuloId: 0,
          tiendaId: 0,
          fecha: new Date(),
        };
      },
      (error) => {
        console.error(error);
      }
    );
  }

  updateArticuloTienda(articuloTienda: ArticuloTienda): void {
    this.apiService.updateArticuloTienda(articuloTienda.id, articuloTienda).subscribe(
      (response) => {
        console.log(response);
        // Actualizar la lista de articulo-tiendas
        this.getAllArticuloTiendas();
      },
      (error) => {
        console.error(error);
      }
    );
  }

  deleteArticuloTienda(id: number): void {
    this.apiService.deleteArticuloTienda(id).subscribe(
      (response) => {
        console.log(response);
        // Actualizar la lista de articulo-tiendas
        this.getAllArticuloTiendas();
      },
      (error) => {
        console.error(error);
      }
    );
  }
}