import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticulotiendaComponent } from './articulotienda.component';

describe('ArticulotiendaComponent', () => {
  let component: ArticulotiendaComponent;
  let fixture: ComponentFixture<ArticulotiendaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticulotiendaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ArticulotiendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
