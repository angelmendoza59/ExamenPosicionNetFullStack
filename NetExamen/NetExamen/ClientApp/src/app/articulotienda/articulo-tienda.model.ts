export interface ArticuloTienda {
    id: number;
    articuloId: number;
    tiendaId: number;
    fecha: Date;
  }