import { Component, OnInit } from '@angular/core';
import { ClienteArticulo } from './cliente-articulo.model';
import { ApiService } from 'service/api.service';




@Component({
  selector: 'app-cliente-articulo',
  templateUrl: './clientearticulo.component.html',
  styleUrls: ['./clientearticulo.component.css']
})
export class ClienteArticuloComponent implements OnInit {
  nuevoClienteArticulo: ClienteArticulo = {
    id: 0,
    clienteId: 0,
    articuloId: 0,
    fecha: new Date(),
    
  };
  clienteArticulos: ClienteArticulo[] = [];

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.getAllClienteArticulos();
  }

  getAllClienteArticulos(): void {
    this.apiService.getAllClienteArticulos().subscribe(
      (response) => {
        this.clienteArticulos = response;
        this.nuevoClienteArticulo = {
          id: 0,
          clienteId: 0,
          articuloId: 0,
          fecha: new Date(),
        }
      },
      (error) => {
        console.error(error);
      }
    );
  }

  addClienteArticulo(): void {
    const clienteArticulos: ClienteArticulo[] = [this.nuevoClienteArticulo]; 
    this.apiService.addClienteArticulo(clienteArticulos).subscribe(
      (response) => {
        this.getAllClienteArticulos()

      },
      (error) => {
        console.error(error);
      }
    );
  }

  updateClienteArticulo(clienteArticulo: ClienteArticulo): void {
    this.apiService.updateClienteArticulo(clienteArticulo).subscribe(
      () => {
        // Actualizar el clienteArticulo en la lista local si es necesario
      },
      (error) => {
        console.error(error);
      }
    );
  }

  deleteClienteArticulo(id: number): void {
    this.apiService.deleteClienteArticulo(id).subscribe(
      () => {
        this.clienteArticulos = this.clienteArticulos.filter(clienteArticulo => clienteArticulo.id !== id);
      },
      (error) => {
        console.error(error);
      }
    );
  }
}
