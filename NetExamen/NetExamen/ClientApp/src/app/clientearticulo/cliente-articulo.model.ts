export interface ClienteArticulo {
    id: number;
    clienteId: number;
    articuloId: number;
    fecha: Date;
  }