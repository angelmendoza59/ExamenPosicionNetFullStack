import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientearticuloComponent } from './clientearticulo.component';

describe('ClientearticuloComponent', () => {
  let component: ClientearticuloComponent;
  let fixture: ComponentFixture<ClientearticuloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientearticuloComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ClientearticuloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
