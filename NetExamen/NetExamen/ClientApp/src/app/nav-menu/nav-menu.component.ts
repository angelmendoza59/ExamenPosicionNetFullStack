import { Component, OnInit } from '@angular/core';
import { map, Observable } from 'rxjs';
import { AuthorizeGuard } from '../../api-authorization/authorize.guard';
import { AuthorizeService } from '../../api-authorization/authorize.service';
import { ApiService } from 'service/api.service';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
  isExpanded = false;
  public isAuthenticated?: boolean;
  public userName?: string | null | undefined;
  public nombre?: string;
  userRole: string | undefined;

  constructor(private authorizeService: AuthorizeService, private apiService: ApiService) { }
  ngOnInit() {
    this.authorizeService.isAuthenticated().subscribe(isAuthenticated => {
      this.isAuthenticated = isAuthenticated;
    });

    this.authorizeService.getUser().subscribe(user => {
      if (user) {
        this.userName = user.name;
        this.userRole = user.role;
      }
    });
    
    const email = this.userName ?? ''; // Asignar un valor predeterminado en caso de que this.userName sea null o undefined

    this.apiService.getClientesByEmailName(email).subscribe(
      (response) => {
        if (response && response.length > 0) {
          this.nombre = response[0].nombre;
        } else {
          this.nombre= ''; // Asignar un valor predeterminado en caso de que no se encuentre ningún cliente
        }
      },
      (error) => {
        console.error(error);
      }
    );
  }
  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
  
}
