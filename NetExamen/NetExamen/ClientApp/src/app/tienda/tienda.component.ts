import { Component, OnInit } from '@angular/core';
import { ApiService } from 'service/api.service';
import { Tienda } from './tienda.model';


@Component({
  selector: 'app-tienda',
  templateUrl: './tienda.component.html',
  styleUrls: ['./tienda.component.css']
})
export class TiendaComponent implements OnInit {
  tiendas: Tienda[] = [];
  nuevaTienda: Tienda = {
    id: 0,
    sucursal: '',
    direccion: ''
  };

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.getAllTiendas();
  }

  getAllTiendas(): void {
    this.apiService.getTiendas().subscribe(
      (response) => {
        this.tiendas = response;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  addTienda(): void {
    this.apiService.addTienda(this.nuevaTienda).subscribe(
      (response) => {
        console.log(response);
        // Actualizar la lista de tiendas
        this.getAllTiendas();
        // Limpiar el formulario
        this.nuevaTienda = {
          id: 0,
          sucursal: '',
          direccion: ''
        };
      },
      (error) => {
        console.error(error);
      }
    );
  }

  updateTienda(tienda: Tienda): void {
    this.apiService.updateTienda(tienda.id, tienda).subscribe(
      (response) => {
        console.log(response);
        // Actualizar la lista de tiendas
        this.getAllTiendas();
      },
      (error) => {
        console.error(error);
      }
    );
  }

  deleteTienda(id: number): void {
    this.apiService.deleteTienda(id).subscribe(
      (response) => {
        console.log(response);
        // Actualizar la lista de tiendas
        this.getAllTiendas();
      },
      (error) => {
        console.error(error);
      }
    );
  }
}