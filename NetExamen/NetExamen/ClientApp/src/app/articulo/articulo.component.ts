import { Component, OnInit } from '@angular/core';
import { ApiService } from 'service/api.service';
import { Articulo } from './articulo.model';
// Asegúrate de proporcionar la ruta correcta al modelo Articulo

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.css']
})
export class ArticuloComponent implements OnInit {
  articulos: Articulo[] = [];
  nuevoArticulo: Articulo = {
    id: 0,
    codigo: '',
    descripcion: '',
    precio: 0,
    imagen: '',
    stock: 0
  };

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.getAllArticulos();
  }

  getAllArticulos(): void {
    this.apiService.getArticulos().subscribe(
      (response) => {
        this.articulos = response;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  addArticulo(): void {
    this.apiService.addArticulo(this.nuevoArticulo).subscribe(
      (response) => {
        console.log(response);
        // Actualizar la lista de artículos
        this.getAllArticulos();
        // Limpiar el formulario
        this.nuevoArticulo = {
          id: 0,
          codigo: '',
          descripcion: '',
          precio: 0,
          imagen: '',
          stock: 0
        };
      },
      (error) => {
        console.error(error);
      }
    );
  }

  updateArticulo(articulo: Articulo): void {
    this.apiService.updateArticulo(articulo.id, articulo).subscribe(
      (response) => {
        console.log(response);
        // Actualizar la lista de artículos
        this.getAllArticulos();
      },
      (error) => {
        console.error(error);
      }
    );
  }

  deleteArticulo(id: number): void {
    this.apiService.deleteArticulo(id).subscribe(
      (response) => {
        console.log(response);
        // Actualizar la lista de artículos
        this.getAllArticulos();
      },
      (error) => {
        console.error(error);
      }
    );
  }
}