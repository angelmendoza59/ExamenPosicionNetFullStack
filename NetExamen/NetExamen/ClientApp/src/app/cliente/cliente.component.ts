import { Component, OnInit } from '@angular/core';

import { Cliente } from './cliente.model';
import { ApiService } from 'service/api.service';


@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {
  clientes: Cliente[] = [];
  nuevoCliente: Cliente = {
    id: 0,
    nombre: '',
    apellidos: '',
    direccion: ''
  };

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getAllClientes();
  }

  getAllClientes() {
    this.apiService.getClientes().subscribe(
      (clientes) => {
        this.clientes = clientes;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  addCliente() {
    this.apiService.addCliente(this.nuevoCliente).subscribe(
      (response) => {
        console.log(response);
        // Actualizar la lista de clientes
        this.getAllClientes();
        // Reiniciar el nuevo cliente
        this.nuevoCliente = {
          id: 0,
          nombre: '',
          apellidos: '',
          direccion: ''
        };
      },
      (error) => {
        console.error(error);
      }
    );
  }

  updateCliente(cliente: Cliente) {
    if (cliente.id) {
      this.apiService.updateCliente(cliente.id, cliente).subscribe(
        (response) => {
          console.log(response);
          // Actualizar la lista de clientes
          this.getAllClientes();
        },
        (error) => {
          console.error(error);
        }
      );
    } else {
      console.error('El cliente no tiene un ID válido');
    }
  }

  deleteCliente(id: number) {
    this.apiService.deleteCliente(id).subscribe(
      (response) => {
        console.log(response);
        // Actualizar la lista de clientes
        this.getAllClientes();
      },
      (error) => {
        console.error(error);
      }
    );
  }
}