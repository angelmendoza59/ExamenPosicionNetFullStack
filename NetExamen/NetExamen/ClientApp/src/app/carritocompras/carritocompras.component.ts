import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Articulo } from '../articulo/articulo.model';
import { CarritoComprasService } from './CarritoComprasService';
import { ApiService } from 'service/api.service';
import { Observable, map } from 'rxjs';
import { AuthorizeService } from 'src/api-authorization/authorize.service';

@Component({
  selector: 'app-carritocompras',
  templateUrl: './carritocompras.component.html',
  styleUrls: ['./carritocompras.component.css']
})
export class CarritoComprasComponent implements OnInit {
  articulos: Articulo[] = [];
  articulosEnCarrito: Articulo[] = [];
  userid: number;
  public isAuthenticated?: boolean;
  public userName?: string | undefined;
  userRole: string | undefined;


  constructor(private carritoService: CarritoComprasService, private apiService: ApiService,private authorizeService: AuthorizeService) {this.userid = 0}

  ngOnInit(): void {
    this.obtenerArticulosEnCarrito();
    this.authorizeService.isAuthenticated().subscribe(isAuthenticated => {
      this.isAuthenticated = isAuthenticated;
    });

    this.authorizeService.getUser().subscribe(user => {
      if (user) {
        this.userName = user.name;
        this.userRole = user.role;
      }
    });
    
    if (this.userName === undefined) {
      this.userName = ''; // Asignar un valor predeterminado en caso de que this.userName sea undefined
    }
    
    
    
  }
  
  obtenerArticulosEnCarrito(): void {
    this.articulosEnCarrito = this.carritoService.obtenerArticulos();
  }

  eliminarArticulo(articulo: Articulo): void {
    this.carritoService.eliminarArticulo(articulo);
    this.obtenerArticulosEnCarrito(); // Actualizar la lista de artículos en el carrito después de eliminar
  }

  buySelectedArticle(): void {
    if (this.userName === undefined) {
      this.userName = ''; // Asignar un valor predeterminado en caso de que this.userName sea undefined
    }
  
    // Obtener el clienteId utilizando el servicio para obtener los clientes por email
    this.apiService.getClientesByEmail(this.userName).subscribe(
      (response) => {
        // Verificar si se obtuvo una respuesta válida
        if (response && response.length > 0) {
          const clienteId = response[0]; // Obtener el primer clienteId de la respuesta (puedes ajustar esto según tu lógica)
  
          // Crear la lista de clienteArticulos con los datos necesarios
          const clienteArticulos = this.articulosEnCarrito.map(articulo => ({
            clienteId: clienteId,
            articuloId: articulo.id,
            fecha: new Date().toISOString() // Obtener la fecha actual en formato ISO
          }));
  
          // Llamar al método addClienteArticulo del servicio para realizar la compra
          this.apiService.addClienteArticulo(clienteArticulos).subscribe(
            (response) => {
              // Aquí puedes manejar la respuesta (si es necesario)
              console.log(response);
  
              // Limpiar el carrito de compras
              this.articulosEnCarrito = [];
  
              // Mostrar una alerta de compra realizada con éxito
              alert('Compra realizada con éxito');
            },
            (error) => {
              // Aquí puedes manejar el error (si es necesario)
              console.error(error);
            }
          );
        } else {
          // No se encontró un cliente con el email proporcionado
          console.error('No se encontró un cliente con el email proporcionado');
        }
      },
      (error) => {
        // Aquí puedes manejar el error al obtener los clientes por email
        console.error(error);
      }
    );
  }
}