import { Injectable } from '@angular/core';
import { Articulo } from '../articulo/articulo.model';

@Injectable({
  providedIn: 'root'
})
export class CarritoComprasService {
  private articulos: Articulo[] = [];

  constructor() {}

  agregarArticulo(articulo: Articulo): void {
    this.articulos.push(articulo);
  }

  eliminarArticulo(articulo: Articulo): void {
    const index = this.articulos.indexOf(articulo);
    if (index !== -1) {
      this.articulos.splice(index, 1);
    }
  }

  obtenerArticulos(): Articulo[] {
    return this.articulos;
  }
  
}