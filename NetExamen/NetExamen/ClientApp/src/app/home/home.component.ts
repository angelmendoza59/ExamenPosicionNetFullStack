import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { Observable, map, of } from 'rxjs';
import { ApiService } from 'service/api.service';
import { AuthorizeService } from 'src/api-authorization/authorize.service';
import { CarritoComprasService } from '../carritocompras/CarritoComprasService';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  articles: any[] = [];
  
  public userName?: Observable<string | null | undefined>;
  public isAuthenticated = false;


  constructor(private apiService: ApiService, private router: Router, private authorizeService: AuthorizeService,private carritoComprasService: CarritoComprasService) { }

  ngOnInit(): void {
    this.getArticles();
    this.authorizeService.isAuthenticated().subscribe(result => {
      this.isAuthenticated = result;
    });
    this.userName = this.authorizeService.getUser().pipe(map(u => u && u.name));
  }

  getArticles(): void {
    this.apiService.getAllArticuloTiendas().subscribe(
      (response: any[]) => {
        this.articles = response;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  buyArticle(articleId: number): void {
    if (this.isLoggedIn()) {
      // Obtener el artículo seleccionado según su ID y cualquier otra lógica necesaria
      const selectedArticle = this.articles.find(article => article.articulo.id === articleId);
  
      if (selectedArticle) {
        // Agregar el artículo al carrito de compras utilizando el servicio correspondiente
        this.carritoComprasService.agregarArticulo(selectedArticle.articulo);
        
        // Redireccionar al componente CarritoCompras
        this.router.navigate(['/carritocompras']);
      }
    } else {
      this.router.navigate(['/authentication/login']);
    }
  }

  isLoggedIn(): boolean {
    return !!this.isAuthenticated && !!this.userName;
  }
  redirectToLogin(): void {
    this.router.navigate(['/authentication/login']);
  }
}