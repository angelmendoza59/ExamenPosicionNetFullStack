import { Component, OnInit } from '@angular/core';
import { AuthorizeService } from '../authorize.service';
import { Observable } from 'rxjs';
import { flatMap, map } from 'rxjs/operators';
import { ApiService } from 'service/api.service';

@Component({
  selector: 'app-login-menu',
  templateUrl: './login-menu.component.html',
  styleUrls: ['./login-menu.component.css']
})
export class LoginMenuComponent implements OnInit {
  public isAuthenticated?: Observable<boolean>;
  public userName?: Observable<string | null | undefined>;
  public nombre?: string;
  public mail?: string;

  constructor(private authorizeService: AuthorizeService, private apiService: ApiService) { }

  ngOnInit() {
    this.isAuthenticated = this.authorizeService.isAuthenticated();
    this.userName = this.authorizeService.getUser().pipe(map(u => u && u.name));
    
    this.userName.subscribe(userName => {
      this.mail = userName ?? ''; // Asignar el valor de userName a this.mail, considerando los casos de null o undefined
      const email = this.mail ?? ''; // Asignar un valor predeterminado en caso de que this.mail sea null o undefined
      console.log(email)
      this.apiService.getClientesByEmailName(email)
          .subscribe(
          (response: any[]) => { // Especificar el tipo de response como any[]
            if (response && response.length > 0) {
              this.nombre = response[0].nombre;
              console.log(response)
            } else {
              this.nombre = ''; // Asignar un valor predeterminado en caso de que no se encuentre ningún cliente
            }
          },
          (error) => {
            console.error(error);
          }
        );
    });
  }
}