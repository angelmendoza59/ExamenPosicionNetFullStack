import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthorizeService } from './authorize.service';
import { tap } from 'rxjs/operators';
import { ApplicationPaths, QueryParameterNames } from './api-authorization.constants';

@Injectable({
  providedIn: 'root'
})
export class AuthorizeGuard implements CanActivate {
  isLoggedIn = false; // Estado de inicio de sesión
  userRole = ''; // Rol asignado
  constructor(private authorize: AuthorizeService, private router: Router) {
  }
  canActivate(
    _next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authorize.isAuthenticated()
      .pipe(tap(isAuthenticated => {
        this.isLoggedIn = isAuthenticated; // Establece el valor de isLoggedIn

        if (isAuthenticated) {
          // Obtiene el rol asignado al usuario
          this.authorize.getUser().subscribe(user => {
            this.userRole = user?.role ?? 'Usuario'; // Establece el valor de userRole
            this.handleAuthorization(isAuthenticated, state);
          });
        } else {
          this.handleAuthorization(isAuthenticated, state);
        }
      }));
  }

  private handleAuthorization(isAuthenticated: boolean, state: RouterStateSnapshot) {
    if (!isAuthenticated) {
      this.router.navigate(ApplicationPaths.LoginPathComponents, {
        queryParams: {
          [QueryParameterNames.ReturnUrl]: state.url
        }
      });
    }
  }
}
