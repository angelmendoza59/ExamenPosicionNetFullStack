﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class ClienteArticulo
    {
        public int Id { get; set; }
        public int ClienteId { get; set; } 
        public int ArticuloId { get; set; }
        public DateTime Fecha { get; set; }

        public Cliente? Cliente { get; set; }
        public Articulo? Articulo { get; set; }
    }
}
