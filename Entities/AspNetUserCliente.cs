﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class AspNetUserCliente
    {
        public string AspNetUserId { get; set; }
        public ApplicationUser AspNetUser { get; set; }

        public int ClienteId { get; set; }
        public Cliente Cliente { get; set; }
    }
}
