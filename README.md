Para ejecutar este programa es necesario primero poner en el appsetings.json, la cadena de conexion al servidor SQL, posteriormente usar los comandos Add-Migration y el comando de Update-Database para que la BD sea creada.
Asi mismo, si es necesario, abrir la solucion Front y en la carpeta clientapp correr el comando npm install.
Luego en el archivo apí.service.ts asegurarse que la direccion de baseurl, corresponda a la direccion del backend.
Despues de registrar el usuario, se habilita la compra de productos.
para registrar tiendas y productos se cuenta con un usuario de admin cuyo login se encuentra en el program.cs y puede modificarse, este tiene acceso a un CRUD total de todas las tablas.
